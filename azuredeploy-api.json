{
    "$schema": "https://schema.management.azure.com/schemas/2019-04-01/deploymentTemplate.json#",
    "contentVersion": "1.0.0.0",
    "parameters": {
        "location": {
            "type": "String"
        },
        "vmssName": {
            "type": "string",
            "minLength": 3,
            "maxLength": 61,
            "metadata": {
                "description": "String used as a base for naming resources. Must be 3-61 characters in length and globally unique across Azure. A hash is prepended to this string for some resources, and resource-specific information is appended."
            }
        },
        "vmSku": {
            "type": "string",
            "defaultValue": "Standard_A1_v2",
            "metadata": {
                "description": "Size of VMs in the VM Scale Set."
            }
        },
        "instanceCount": {
            "type": "int",
            "defaultValue": 2,
            "minValue": 1,
            "maxValue": 10,
            "metadata": {
                "description": "Number of VM instances (100 or less)."
            }
        },
        "adminUsername": {
            "type": "string",
            "defaultValue": "vmssadmin",
            "metadata": {
                "description": "Admin username on all VMs."
            }
        },
        "adminPassword": {
            "type": "securestring",
            "metadata": {
                "description": "Admin password on all VMs."
            }
        },
        "virtualNetworkName": {
            "type": "string",
            "metadata": {
                "description": "vnet for lb ss."
            }
        },
        "subnetName": {
            "type": "string",
            "metadata": {
                "description": "subnet for lb ss"
            }
        },
        "LbFrontIp": {
            "type": "string"
        },
        "portNumber": {
            "type": "int",
            "defaultValue": "[parameters('portNumber')]"
        },
        "autoScaleCapacity": {
            "type": "array",
            "defaultValue": [ "1", "10", "2" ]
        },
        "command": {
            "type": "string"
        },
        "urlScript": {
            "type": "array"
        },
        "storageAccountName": {
            "type": "string"
        },
        "storageAccountKey": {
            "type": "string"
        }
    },
    "functions": [],
    "variables": {
        "namingInfix": "[parameters('vmssName')]",
        "vmss": "[concat('vmss',variables('namingInfix'))]",
        "nicName": "[concat('nic',variables('namingInfix'))]",
        "ipConfigName": "[concat('ipconfig',variables('namingInfix'))]",
        "loadBalancerName": "[concat('LB',variables('namingInfix'))]",
        "bePoolName": "[concat('bepool',variables('namingInfix'))]",
        "lbProbeID": "[resourceId('Microsoft.Network/loadBalancers/probes',variables('loadBalancerName'), 'tcpProbe')]",
        "lbPoolID": "[resourceId('Microsoft.Network/loadBalancers/backendAddressPools',variables('loadBalancerName'),variables('bePoolName'))]",
        "frontEndIPConfigID": "[resourceId('Microsoft.Network/loadBalancers/frontendIPConfigurations',variables('loadBalancerName'),'loadBalancerFrontEnd')]",
        "autoscalesettings_vmss_name": "[concat(variables('namingInfix'), 'as-vmss')]",
        "linuxImage": {
            "publisher": "Canonical",
            "offer": "UbuntuServer",
            "sku": "18_04-lts-gen2",
            "version": "latest"
        }

    },
    "resources": [
        {
            "type": "microsoft.insights/autoscalesettings",
            "apiVersion": "2015-04-01",
            "name": "[variables('autoscalesettings_vmss_name')]",
            "location": "[parameters('location')]",
            "dependsOn": [
                "[resourceId('Microsoft.Compute/virtualMachineScaleSets/', variables('vmss'))]"
            ],
            "properties": {
                "name": "[variables('autoscalesettings_vmss_name')]",
                "targetResourceUri": "[resourceId('Microsoft.Compute/virtualMachineScaleSets', variables('vmss'))]",
                "enabled": true,
                "profiles": [
                    {
                        "name": "[variables('autoscalesettings_vmss_name')]",
                        "capacity": {
                            "minimum": "[parameters('autoScaleCapacity')[0]]",
                            "maximum": "[parameters('autoScaleCapacity')[1]]",
                            "default": "[parameters('autoScaleCapacity')[2]]"
                        },
                        "rules": [
                            {
                                "metricTrigger": {
                                    "metricName": "Percentage CPU",
                                    "metricResourceUri": "[resourceId('Microsoft.Compute/virtualMachineScaleSets', variables('vmss'))]",
                                    "timeGrain": "PT1M",
                                    "statistic": "Average",
                                    "timeWindow": "PT10M",
                                    "timeAggregation": "Average",
                                    "operator": "GreaterThan",
                                    "threshold": 75,
                                    "dividePerInstance": false
                                },
                                "scaleAction": {
                                    "direction": "Increase",
                                    "type": "ChangeCount",
                                    "value": "1",
                                    "cooldown": "PT1M"
                                }
                            },
                            {
                                "metricTrigger": {
                                    "metricName": "Percentage CPU",
                                    "metricResourceUri": "[resourceId('Microsoft.Compute/virtualMachineScaleSets', variables('vmss'))]",
                                    "timeGrain": "PT1M",
                                    "statistic": "Average",
                                    "timeWindow": "PT5M",
                                    "timeAggregation": "Average",
                                    "operator": "LessThan",
                                    "threshold": 25,
                                    "dividePerInstance": false
                                },
                                "scaleAction": {
                                    "direction": "Decrease",
                                    "type": "ChangeCount",
                                    "value": "1",
                                    "cooldown": "PT1M"
                                }
                            }
                        ]
                    }
                ]
            }
        },
        {
            "type": "Microsoft.Network/loadBalancers",
            "apiVersion": "2020-11-01",
            "name": "[variables('loadBalancerName')]",
            "location": "[parameters('location')]",
            "dependsOn": [],
            "tags": {
                "Team": "[variables('namingInfix')]"
            },
            "sku": {
                "name": "basic",
                "tier": "Regional"
            },
            "properties": {
                "frontendIPConfigurations": [
                    {
                        "name": "LoadBalancerFrontEnd",
                        "properties": {
                            "privateIPAddress": "[parameters('LbFrontIp')]",
                            "privateIPAllocationMethod": "static",
                            "subnet": {
                                "id": "[resourceId('Microsoft.Network/virtualNetworks/subnets', parameters('virtualNetworkName'), parameters('subnetName'))]"
                            },
                            "privateIPAddressVersion": "IPv4"
                        },
                        "zones": [
                            "1"
                        ]
                    }
                ],
                "backendAddressPools": [
                    {
                        "name": "[variables('bePoolName')]"
                    }
                ],
                "loadBalancingRules": [
                    {
                        "name": "[concat('lb_r' ,variables('namingInfix'))]",
                        "properties": {
                            "frontendIPConfiguration": {
                                "id": "[variables('frontEndIPConfigID')]"
                            },
                            "frontendPort": "[parameters('portNumber')]",
                            "backendPort": "[parameters('portNumber')]",
                            "enableFloatingIP": false,
                            "idleTimeoutInMinutes": 4,
                            "protocol": "Tcp",
                            "enableTcpReset": false,
                            "loadDistribution": "Default",
                            "disableOutboundSnat": true,
                            "backendAddressPool": {
                                "id": "[variables('lbPoolID')]"
                            },
                            "probe": {
                                "id": "[variables('lbProbeID')]"
                            }
                        }
                    }
                ],
                "probes": [
                    {
                        "name": "tcpProbe",
                        "properties": {
                            "protocol": "Tcp",
                            "port": "[parameters('portNumber')]",
                            "intervalInSeconds": 5,
                            "numberOfProbes": 2
                        }
                    }
                ],
                "inboundNatRules": [],
                "outboundRules": [],
                "inboundNatPools": []
            }
        },
        {
            "type": "Microsoft.Compute/virtualMachineScaleSets",
            "apiVersion": "2021-03-01",
            "name": "[variables('vmss')]",
            "location": "[parameters('location')]",
            "dependsOn": [
                "[resourceId('Microsoft.Network/loadBalancers', variables('loadBalancerName'))]"
            ],
            "tags": {
                "Team": "[variables('namingInfix')]"
            },
            "sku": {
                "name": "[parameters('vmSku')]",
                "tier": "Standard",
                "capacity": "[parameters('instanceCount')]"
            },
            "properties": {
                "singlePlacementGroup": true,
                "upgradePolicy": {
                    "mode": "Automatic"
                },
                "scaleInPolicy": {
                    "rules": [
                        "OldestVM"
                    ]
                },
                "virtualMachineProfile": {
                    "osProfile": {
                        "computerNamePrefix": "[variables('namingInfix')]",
                        "adminUsername": "[parameters('adminUsername')]",
                        "adminPassword": "[parameters('adminPassword')]"
                    },
                    "storageProfile": {
                        "osDisk": {
                            "createOption": "FromImage",
                            "caching": "ReadWrite"
                        },
                        "imageReference": "[variables('linuxImage')]"
                    },
                    "networkProfile": {
                        "networkInterfaceConfigurations": [
                            {
                                "name": "[variables('nicName')]",
                                "properties": {
                                    "primary": true,
                                    "ipConfigurations": [
                                        {
                                            "name": "[variables('ipConfigName')]",
                                            "properties": {
                                                "primary": true,
                                                "subnet": {
                                                    "id": "[resourceId('Microsoft.Network/virtualNetworks/subnets', parameters('virtualNetworkName'), parameters('subnetName'))]"
                                                },
                                                "privateIPAddressVersion": "IPv4",
                                                "loadBalancerBackendAddressPools": [
                                                    {
                                                        "id": "[variables('lbPoolID')]"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    },
                    "diagnosticsProfile": {
                        "bootDiagnostics": {
                            "enabled": true
                        }
                    },
                    "extensionProfile": {
                        "extensions": [
                            {
                                "name": "healthRepairExtension",
                                "properties": {
                                    "autoUpgradeMinorVersion": true,
                                    "publisher": "Microsoft.ManagedServices",
                                    "type": "ApplicationHealthLinux",
                                    "typeHandlerVersion": "1.0",
                                    "settings": {
                                        "protocol": "tcp",
                                        "port": "[parameters('portNumber')]"
                                    }
                                }
                            },

                            {
                                "name": "AppInstall",
                                "properties": {
                                    "publisher": "Microsoft.Azure.Extensions",
                                    "type": "CustomScript",
                                    "typeHandlerVersion": "2.0",
                                    "autoUpgradeMinorVersion": true,
                                    "forceUpdateTag": "v1.0",
                                    "protectedSettings": {
                                        "storageAccountName": "[parameters('storageAccountName')]",
                                        "storageAccountKey": "[parameters('storageAccountKey')]",
                                        "fileUris": "[parameters('urlScript')]",
                                        "commandToExecute": "[parameters('command')]"
                                    }
                                }
                            }
                        ]
                    }
                },
                "overprovision": true,
                "doNotRunExtensionsOnOverprovisionedVMs": false,
                "platformFaultDomainCount": 5,
                "automaticRepairsPolicy": {
                    "enabled": true,
                    "gracePeriod": "PT10M"
                }
            },
                        "zones": [
                            "1"
                        ]
        }
        
    ],
    "outputs": {}
}
