$rg='rg-team-2-sep-case-arm-we'
$job='case-rk.' + ((Get-Date).ToUniversalTime()).tostring("yyyyMMdd.HHmm")

# Deploy VNet with application subnets
$templateVNet='.\team-2-sep\azuredeploy-vnet.json'
$paramsVNet='.\team-2-sep\azuredeploy-vnet.parameters.json'

# Deploy Keyvault with MongoDB secret
$templateKV='.\team-2-sep\azuredeploy-kv.json'
$paramsKV='.\team-2-sep\azuredeploy-kv.parameters.json'

# Deploy MongoDB Virtual Machine
$templateVM='.\team-2-sep\azuredeploy-vm-mongodb.json'
$paramsVM='.\team-2-sep\azuredeploy-vm-mongodb.parameters.json'
$paramsVM='.\team-2-sep\azuredeploy-vm-mongodb.parameters.repoforteam2.json'

# Deploy Bastion subnet
$templateBast='.\team-2-sep\azuredeploy-bastion.json'
$paramsBast='.\team-2-sep\azuredeploy-bastion.parameters.json'

# Deploy RSV
$templateRSV='.\team-2-sep\azuredeploy-rsv.json'
$paramsRSV='.\team-2-sep\azuredeploy-rsv.parameters.json'

# Deploy Gateway PIP
$templatePIPgw='.\team-2-sep\azuredeploy-pip-gw.json'
$paramsPIPgw='.\team-2-sep\azuredeploy-pip-gw.parameters.json'

# Deploy Log Analytics Workspace
$templateLAW='.\team-2-sep\azuredeploy-law.json'
$paramsLAW='.\team-2-sep\azuredeploy-law.parameters.json'

# Deploy Log Analytics Workspace
$templateLAA='.\team-2-sep\azuredeploy-la-agent-linux.json'
$paramsLAA='.\team-2-sep\azuredeploy-la-agent-linux.parameters.mongo.json'

# Deploy App Gateway
$templateGW='.\team-2-sep\azuredeploy-app-gw.json'
$paramsGW='.\team-2-sep\azuredeploy-app-gw.parameters.json'

az deployment group create --name $job --resource-group $rg --template-file $templateVNet --parameters "@$paramsVNet"
az deployment group create --name $job --resource-group $rg --template-file $templateKV --parameters "@$paramsKV"
az deployment group create --name $job --resource-group $rg --template-file $templateVM --parameters "@$paramsVM"
az deployment group create --name $job --resource-group $rg --template-file $templateBast --parameters "@$paramsBast"
az deployment group create --name $job --resource-group $rg --template-file $templatePIPgw --parameters "@$paramsPIPgw" --what-if
az deployment group create --name $job --resource-group $rg --template-file $templateLAW --parameters "@$paramsLAW"
az deployment group create --name $job --resource-group $rg --template-file $templateLAA --parameters "@$paramsLAA"
az deployment group create --name $job --resource-group $rg --template-file $templateGW --parameters "@$paramsGW"

# Get IP address of PIP just added  -- Now done in PIP template:
# $ip=az network public-ip show --resource-group $rg --name 'pip-sep-t2-arm-we-app-gw' --query "{address: ipAddress}" --output tsv
# az network dns record-set a add-record --resource-group rg-tap-core-we -z azure.nc-tap.com -n 'team2-prod' -a $ip

# Create Recovery Services Vault
az deployment group create --name $job --resource-group $rg --template-file $templateRSV --parameters "@$paramsRSV"
# Enable backup for VM - should be able to add to ARM??
$vault=az backup vault list -g $rg --query "[].{Name:name}" --output tsv
$vm=az vm list -g $rg --query "[?contains(name, 'mongo')].{Name:name}" --output tsv
$policy=az backup policy list -g $rg -v $vault --query "[?contains(name, 'sep-t2')].{Name:name}" --output tsv

az backup protection enable-for-vm --resource-group $rg --vault $vault --vm $vm --policy-name $policy

